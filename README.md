XKB EurKEY for ISO Layout
=========================

The EurKEY (US ISO) is a variant of the [EurKEY][eu] layout shipped with
[xkeyboard-config][xkbc] for keyboards with ISO-Layout.

[eu]: https://gitlab.freedesktop.org/xkeyboard-config/xkeyboard-config/-/blob/master/symbols/eu

[xkbc]: https://gitlab.freedesktop.org/xkeyboard-config/xkeyboard-config

![preview](preview.png)

Usage
-----

To install the layout, run as `root`:

```sh
./install.py install -x
```

To uninstall run:

```sh
./install.py uninstall -x
```

Rationale
---------

There are a great variety of nice key-cap sets, but the majority target
ANSI-layout keyboards. However, many include additional caps for using the set
on ISO-layout keyboards, i.e. vertical *Return* and smaller *left Shift* keys
and two additional normal-sized keys which correspond to the UK layout. Since
most key-caps have different profiles for different rows, the key-caps can't be
freely swapped around. Therefore, most of those key-cap sets aren't fully
compatible with the EurKEY or similar layouts like English (International).

This layout, however, is compatible with most US key-cap sets with additional
keys for the ISO layout.

|        LSGT        |         BKSL          |
|:------------------:|:---------------------:|
| ![](backslash.png) |  ![](numbersign.png)  |
| backslash and bar  | number sign and tilde |
