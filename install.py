#!/usr/bin/env python
#
# Copyright (C) 2021 Max Harmathy <harmathy@mailbox.org>
#
# This program is free software: you can redistribute it and/or modify it und
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
installer for xkb layout
"""

import copy
import os
import sys
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from pathlib import Path
from shutil import copyfile
from typing import Any
from typing import Final
from typing import Iterator
from typing import List
from typing import Optional
from typing import Union

import pytest
from lxml import etree
from lxml.etree import Element
from lxml.etree import ElementTree

# types

ListOrString = Union[List[str], str, None]

# constants

HERE: Final[str] = os.path.dirname(os.path.abspath(__file__))


class Layout:

    def __init__(self,
                 name: str,
                 description: str,
                 language_list: ListOrString = None,
                 short_description: Optional[str] = None) -> None:
        self._language_list = None
        self._language_string = None
        self.name = name
        self.description = description
        self.short_description = short_description if short_description is not None else name
        self.language_list = language_list

    def to_element(self) -> Element:
        result = Element('layout')

        config_item = Element('configItem')
        result.append(config_item)

        name = Element('name')
        name.text = self.name
        config_item.append(name)

        short_description = Element('shortDescription')
        short_description.text = self.short_description
        config_item.append(short_description)

        description = Element('description')
        description.text = self.description
        config_item.append(description)

        if len(self.language_list) > 0:
            language_list = Element('languageList')
            config_item.append(language_list)
            for item in self.language_list:
                language = Element('iso639Id')
                language.text = item
                language_list.append(language)

        return result

    @property
    def language_list(self) -> List[str]:
        return self._language_list

    @language_list.setter
    def language_list(self, languages: ListOrString) -> None:
        if languages is None:
            self._language_list = []
            self._language_string = ""
        elif isinstance(languages, list):
            self._language_list = languages
            self._language_string = ",".join(languages)
        elif isinstance(languages, str):
            self._language_list = languages.split(',')
            self._language_string = languages
        else:
            raise TypeError(f"language_list is a {type(languages).__name__}, but should be str or list.")

    @property
    def language_string(self) -> str:
        return self._language_string


EU_ISO = Layout('eu_iso', 'EurKEY (US ISO)', ['eng', 'ger'])


def add_layout(layout_list: Element, layout: Layout) -> None:
    if not has_layout(layout_list, layout):
        layout_list.append(layout.to_element())


def has_layout(layout_list: Element, layout: Layout) -> bool:
    return any(xml_element_equal(item, layout.to_element()) for item in layout_list)


def remove_layout(layout_list: Element, layout: Layout) -> None:
    layout_element = layout.to_element()
    for item in layout_list:
        if xml_element_equal(item, layout_element):
            layout_list.remove(item)


def get_layout_list(element_tree: ElementTree) -> Element:
    element = element_tree.getroot()
    tag = 'layoutList'
    assert any(child.tag == tag for child in element)
    candidates = [item for item in element if item.tag == tag]
    assert len(candidates) == 1
    layout_list = candidates.pop()
    return layout_list


def parse_xml_file(xml_file: str) -> ElementTree:
    parser = etree.XMLParser(encoding="utf-8", remove_blank_text=True)
    return etree.parse(source=xml_file, parser=parser)


def xml_element_attribute_equality(first: Element, second: Element, attribute: Any) -> bool:
    attribute1 = getattr(first, attribute)
    attribute2 = getattr(second, attribute)
    return attribute1 == attribute2 or (
        (attribute1 is None or attribute1.isspace()) and
        (attribute2 is None or attribute2.isspace())
    )


def xml_element_equal(first: Element, second: Element) -> bool:
    return first.tag == second.tag \
        and xml_element_attribute_equality(first, second, 'text') \
        and xml_element_attribute_equality(first, second, 'tail') \
        and first.attrib == second.attrib \
        and all(xml_element_equal(child1, child2)
                for (child1, child2)
                in zip(first, second))


# Test Code

def xml_diff(first: Element, second: Element) -> Iterator[str]:
    if first.tag != second.tag:
        yield f'tag: {first.tag} <> {second.tag}'
    if not xml_element_attribute_equality(first, second, 'text'):
        yield f'text ({first.tag}) {first.text} <> {second.text}'
    if not xml_element_attribute_equality(first, second, 'tail'):
        yield f'tail ({first.tag}) {first.tail} <> {second.tail}'
    if first.attrib != second.attrib:
        yield f'attrib ({first.tag}) {first.attib} <> {second.attrib}'
    for (child1, child2) in zip(first, second):
        for diff in xml_diff(child1, child2):
            yield f'|-{diff}'


def test_add_and_remove(tmp_path: Path) -> None:
    content = parse_xml_file(f"{HERE}/test/evdev.xml.in")
    original = copy.deepcopy(content)
    layout_list = get_layout_list(content)
    add_layout(layout_list, EU_ISO)
    expected = etree.parse(f"{HERE}/test/evdev.xml.out")
    out_file = tmp_path / 'output.xml'
    content.write(str(out_file), encoding='utf-8', xml_declaration=True, pretty_print=True)
    for diff in xml_diff(content.getroot(), expected.getroot()):
        print(diff, file=sys.stderr)
    assert xml_element_equal(content.getroot(), expected.getroot())
    remove_layout(layout_list, EU_ISO)
    assert xml_element_equal(content.getroot(), original.getroot())


def test_layout_language_list() -> None:
    test_string = "a,b,c,d,e,f,g"
    test_list = test_string.split(',')
    subject = Layout(name="test", description="only testing", language_list=None)
    assert subject.language_list == []
    assert subject.language_string == ""

    subject.language_list = test_list
    assert subject.language_list == test_list
    assert subject.language_string == test_string

    subject.language_list = test_string
    assert subject.language_list == test_list
    assert subject.language_string == test_string

    with pytest.raises(TypeError):
        subject.language_list = 4.2


# commandline interface

def list_to_text(a_list: List[str]) -> str:
    list_length = len(a_list)
    if list_length == 0:
        return ""
    elif list_length == 1:
        return f"'{a_list[0]}'"
    elif list_length == 2:
        return f"'{a_list[0]}' or '{a_list[1]}'"
    else:
        return f"'{a_list[0]}', {list_to_text(a_list[1:])}"


def full_path(base: str, prefix: str) -> str:
    if os.path.isabs(base):
        return base
    return os.path.join(prefix, base)


def main() -> int:
    default_registry_path = "rules/evdev.xml"
    xkb_path_meta_variable = "XKB_PATH"
    layout_file_meta_variable = 'FILE'
    default_xkb_path = "/usr/share/X11/xkb"
    parser = ArgumentParser(formatter_class=RawDescriptionHelpFormatter, description=__doc__)
    status_command = 'status'
    commands = ['install', 'uninstall', status_command]
    parser.add_argument(
        "command", metavar="command", choices=commands, default=status_command,
        help=f"one command of {list_to_text(commands)} (default: '{status_command}')"
    )
    parser.add_argument(
        "-p", "--xkb-path", metavar=xkb_path_meta_variable, default=default_xkb_path,
        help=f"base directory of XKB configuration files (default: {default_xkb_path})"
    )
    parser.add_argument(
        '-r', "--registry", default=default_registry_path,
        help=f"location (relative to {xkb_path_meta_variable} or absolute) of the xkb registry file to install the "
             f"layout into (default: {default_registry_path})"
    )
    parser.add_argument(
        "-f", "--layout-file", metavar=layout_file_meta_variable, default=EU_ISO.name,
        help=f"specify a layout file to install (default: '{EU_ISO.name}')"
    )
    parser.add_argument(
        "-d", "--layout-description", metavar='DESCRIPTION', default=EU_ISO.description,
        help=f"specify a description for the layout (default: '{EU_ISO.description}')"
    )
    parser.add_argument(
        '-l', "--layout-languages", metavar="LANGUAGES", default=EU_ISO.language_string,
        help=f"comma separated list of languages, in which the layout will appear "
             f"(default: '{EU_ISO.language_string}')"
    )
    parser.add_argument(
        '-s', "--layout-short-description", metavar='SHORT', default=EU_ISO.name,
        help=f"an short description (if not set, the layout file name {layout_file_meta_variable} is used)"
    )
    parser.add_argument(
        '-x', "--handle-layout-file", action='store_true',
        help=f"copy layout file to XKB directory on install or remove it on uninstall"
    )

    parameters = parser.parse_args()

    if not os.path.exists(parameters.layout_file):
        print("Couldn't find layout file!", file=sys.stderr)
        return 1

    layout_base_name = os.path.basename(parameters.layout_file)

    layout = Layout(
        name=layout_base_name,
        description=parameters.layout_description,
        language_list=parameters.layout_languages,
        short_description=parameters.layout_short_description
    )
    layout_file_in_xkb_dir = os.path.join(parameters.xkb_path, 'symbols', layout_base_name)

    registry = full_path(parameters.registry, parameters.xkb_path)

    if not os.path.exists(registry):
        print("Couldn't find xkb registry file!", file=sys.stderr)
        return 1

    registry_content = parse_xml_file(registry)
    registry_layout_list = get_layout_list(registry_content)

    if parameters.command == status_command:
        file_status = 'exists' if os.path.exists(layout_file_in_xkb_dir) else 'does not exist'
        print(f"Layout: file {layout_file_in_xkb_dir} {file_status}.")
        maybe = '' if has_layout(registry_layout_list, layout) else ' not'
        print(f"XKB Registry: layout {layout.name} is{maybe} installed.")

    else:
        if not os.access(registry, os.W_OK):
            print("XKB registry isn't writable!", file=sys.stderr)
            return 1

        if parameters.command == 'install':
            if parameters.handle_layout_file:
                copyfile(parameters.layout_file, layout_file_in_xkb_dir)
            add_layout(registry_layout_list, layout)
        elif parameters.command == 'uninstall':
            if parameters.handle_layout_file:
                os.remove(layout_file_in_xkb_dir)
            remove_layout(registry_layout_list, layout)
        else:
            print("unimplemented command", file=sys.stderr)
            return 2

        registry_content.write(registry, encoding='utf-8', xml_declaration=True, pretty_print=True)

    return 0


if __name__ == '__main__':
    sys.exit(main())
